# LAB NODEJS HW3
Simple app created with Node.js, MongoDB and Mongoose

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This project UBER like service for freight trucks. This service should help regular people to deliver their stuff and help drivers to find loads and earn some money. 

## Technologies
Project is created with:
* Node.js version: 14.17.3
* Express version: 4.17.1
* Morgan version: 5.13.2
* Mongoose version: 1.10.0

## Setup
To run this project just after git pull, install it locally using npm:

```
$ npm install
$ npm start
```