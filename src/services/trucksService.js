const mongoose = require('mongoose');
const { Truck } = require('../models/truckModel');
const { User } = require('../models/userModel');

const { InvalidRequestError } = require('../utils/errors');

const getTrucksByUserId = async (created_by) => {
    const trucks = await Truck.find({created_by}, '-__v')   
    return trucks; 
}

const addTruckForUser = async (created_by, truckPayload) => {
    const hasTruck = await Truck.findOne({assigned_to: created_by, status: 'OL'});
    if (hasTruck) {
        throw new InvalidRequestError('You are not able to change trucks info while you are on a load');
    }
    const truck = new Truck({...truckPayload, created_by});
    await truck.save();
}

const getTruckByIdForUser = async (truck_id, created_by) => {
    const truck = await Truck.findOne({_id: truck_id, created_by}, '-__v');
    return truck;
}

const updateTruckByIdForUser = async (truck_id, created_by, data) => {
    const hasTruck = await Truck.findOne({assigned_to: created_by, status: 'OL'});
    if (hasTruck) {
        throw new InvalidRequestError('You are not able to change trucks info while you are on a load');
    }
    await Truck.findByIdAndUpdate({_id: truck_id, created_by}, {$set: data});
}

const deleteTruckByIdForUser = async (truck_id, created_by) => {
    const hasTruck = await Truck.findOne({assigned_to: created_by, status: 'OL'});
    if (hasTruck) {
        throw new InvalidRequestError('You are not able to change trucks info while you are on a load');
    }
    await Truck.findOneAndRemove({_id: truck_id, created_by});
}

const assignTruckByIdForUser = async (truck_id, created_by) => {
    const hasTruck = await Truck.findOne({assigned_to: created_by, status: 'OL'});
    if (hasTruck) {
        throw new InvalidRequestError('You are not able to change trucks info while you are on a load');
    }
    const truck = await Truck.findOne({_id: truck_id, created_by});
    const { assigned_to } = truck;
    if (assigned_to !== null) {
        throw new InvalidRequestError('Truck is assigned already');
    }
    const trucks = await Truck.find({assigned_to: created_by});
    if (trucks.length !== 0) {
        throw new InvalidRequestError('User has assigment already');
    }
    await Truck.findByIdAndUpdate({_id: truck_id, created_by}, {$set: {assigned_to: created_by}});
}

module.exports = {
    getTrucksByUserId,
    addTruckForUser,
    getTruckByIdForUser,
    updateTruckByIdForUser,
    deleteTruckByIdForUser,
    assignTruckByIdForUser
}
