const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { User } = require('../models/userModel');

const { CredentialsError } = require('../utils/errors');

const registration = async ({role, email, password}) => {
    const user = new User({
        role, 
        email,
        password: await bcrypt.hash(password, 10)
    });
    await user.save();
}

const logIn = async ({email, password}) => {
    const user = await User.findOne({email});

    if (!user) {
        throw new CredentialsError('Invalid password or username');
    }

    if (!(await bcrypt.compare(password, user.password))) {
        throw new CredentialsError('Invalid password or username');
    }

    const token = jwt.sign({
        _id: user._id,
        email: user.email,
        role: user.role
    }, 'secret');
    return token;
}

module.exports = {
    registration,
    logIn
}