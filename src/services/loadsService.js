const mongoose = require('mongoose');
const { Load } = require('../models/loadModel');
const { Truck } = require('../models/truckModel');
const { User } = require('../models/userModel');

const getLoadsByUserId = async (created_by, role, limit=10, offset=0) => {
    if (role === 'SHIPPER') {
        const loads = await Load.find({created_by}, '-__v')
            .skip(offset)
            .limit(limit);   
        return loads;
    }
    const loads = await Load.find({assigned_to: created_by}, '-__v')
        .skip(offset)
        .limit(limit);
    return loads; 
}

const addLoadForUser = async (created_by, loadPayload) => {
    const load = new Load({...loadPayload, created_by});
    await load.save();
    await load.update({$push: {logs: {
        message: `Load was created by shipper with id ${created_by}`, 
        time: new Date(Date.now())
    }}});
}

const getUserActiveLoad = async (created_by) => {
    const load = await Load.findOne({assigned_to: created_by});
    load.update({$set: {created_by: load.created_by}});
    return await Load.findOne({assigned_to: created_by}, '-__v');; 
}

const iterateToNextLoadState = async (created_by) => { 
    const truck = await Truck.findOne({created_by});
    const load = await Load.findOne({assigned_to: created_by});

    switch (true) {
        case load.state === 'Arrived to delivery':
            throw new Error('Load was arrived to delivery');

        case load.state === 'En route to Pick Up':
            await load.update({$set: {state: 'Arrived to Pick Up'}});
            await load.update({$push: {logs: {
                message: 'Arrived to Pick Up', 
                time: new Date(Date.now())
            }}});
            return await Load.findOne({assigned_to: created_by});

        case load.state === 'Arrived to Pick Up':
            await load.update({$set: {state: 'En route to delivery', status: 'SHIPPED'}});
            await load.update({$push: {logs: {
                message: 'En route to delivery', 
                time: new Date(Date.now())
            }}});
            return await Load.findOne({assigned_to: created_by});
    
        case load.state === 'En route to delivery':
            await load.update({$set: {state: 'Arrived to delivery'}});
            await load.update({$push: {logs: {
                message: 'Arrived to delivery', 
                time: new Date(Date.now())
            }}});
            await truck.update({$set: {status: 'IS'}});
            return await Load.findOne({assigned_to: created_by});
    }
}

const getLoadByIdForUser = async (load_id, created_by, role) => {
    if (role === 'DRIVER') {
        const load = await Load.findOne({_id: load_id, assigned_to: created_by}, '-__v');
        return load;
    }
    const load = await Load.findOne({_id: load_id, created_by}, '-__v');
    return load;
}

const updateLoadByIdForUser = async (load_id, created_by, data) => {
    const load = await Load.findByIdAndUpdate({_id: load_id, created_by}, {$set: data});
    
    await load.update({$push: {logs: {
        message: `Load was updated by shipper with id ${created_by}`, 
        time: new Date(Date.now())
    }}});
}

const deleteLoadByIdForUser = async (load_id, created_by) => {
    await Load.findOneAndRemove({_id: load_id, created_by});
}

const postLoadByIdForUser = async (load_id, created_by) => {
    const load = await Load.findByIdAndUpdate({_id: load_id, created_by}, {$set: {status: 'POSTED'}});
    const trucks = await Truck.find({status: 'IS', assigned_to: { $lt: 'not a number' }}).exec();

    switch (true) {
        case (load.payload<=1700 
        && load.dimensions.width<=300 && load.dimensions.length<=250 && load.dimensions.height<=170): 
            const driver = await findDriver(load, trucks[0]);
            return driver;

        case (load.payload<=2500 
            && load.dimensions.width<=500 && load.dimensions.length<=250 && load.dimensions.height<=170): 
            const truckSm = trucks.find(item => item.type === '!SPRINTER');
            const driverSm = await findDriver(load, truckSm);
            return driverSm;

        case (load.payload<=4000 
            && load.dimensions.width<=700 && load.dimensions.length<=350 && load.dimensions.height<=200): 
            const truckLg = trucks.find(item => item.type === 'LARGE STRAIGHT');
            const driverLg = await findDriver(load, truckLg);
            return driverLg;

        case (load.payload>4000 
        || load.dimensions.width>700 || load.dimensions.length>350 || load.dimensions.height>200):
            await load.update({$push: {logs: {
                message: 'Driver is not found', 
                time: new Date(Date.now())
            }}});
            return null;
    }
}

const getLoadShippingInfoByIdForUser = async (load_id, created_by) => {
    const load = await Load.findOne({_id: load_id, created_by}, '-__v');
    const truck = await Truck.findOne({assigned_to: load.assigned_to}, '-__v');
    const loadInfo = {
        load: load,
        truck: truck
    };
    console.log(loadInfo);
    return loadInfo;
}

const findDriver = async (load, truck) => {
    if (!truck) {
        await load.update({$set: {status: 'NEW'}});
        await load.update({$push: {logs: {
            message: `Driver is not found`, 
            time: new Date(Date.now())
        }}});
        return null;
    }
    await truck.update({$set: {status: 'OL'}});
    await load.update({$set: {
        assigned_to: truck.assigned_to,
        status: 'ASSIGNED', 
        state: 'En route to Pick Up' 
    }});
    await load.update({$push: {logs: {
        message: `Load assigned to driver with id ${truck.assigned_to}`, 
        time: new Date(Date.now())
    }}});
    return truck;
}

module.exports = {
    getLoadsByUserId,
    addLoadForUser,
    getUserActiveLoad,
    iterateToNextLoadState,
    getLoadByIdForUser,
    updateLoadByIdForUser,
    deleteLoadByIdForUser,
    postLoadByIdForUser,
    getLoadShippingInfoByIdForUser
}