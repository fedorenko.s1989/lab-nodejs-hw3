const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const { User } = require('../models/userModel');
const { Truck } = require('../models/truckModel');

const { InvalidRequestError } = require('../utils/errors');

const getUserById = async (created_by) => {
    const user = await User.findOne({_id: created_by}, '-__v -password');   
    return user; 
}

const deleteUserById = async (created_by) => {
    await User.findOneAndRemove({_id: created_by});
}

const changeUserPasswordById = async (created_by, oldPassword, newPassword) => {
    const user = await User.findOne({_id: created_by});

    const hasTruck = await Truck.findOne({assigned_to: created_by, status: 'OL'});
    if (hasTruck) {
        throw new InvalidRequestError('You are not able to change your profile while you are on a load');
    }
    
    if (!(await bcrypt.compare(oldPassword, user.password))) {
        throw new InvalidRequestError('Invalid oldpassword');
    }
    
    await User.findByIdAndUpdate({_id: created_by}, {$set: {password: await bcrypt.hash(newPassword, 10)}});
}

module.exports = {
    getUserById,
    deleteUserById,
    changeUserPasswordById
}
