const express = require('express');
const path = require('path');
const morgan = require('morgan');
const mongoose = require('mongoose');
const HOST = require('dotenv').config();
const app = express();

const { trucksRouter } = require('./controllers/trucksController');
const { authRouter } = require('./controllers/authController');
const { usersRouter } = require('./controllers/usersController');
const { loadsRouter } = require('./controllers/loadsController');
const {authMiddleware} = require('./middlewares/authMiddleware');


app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/trucks', [authMiddleware], trucksRouter);
app.use('/api/loads', [authMiddleware], loadsRouter);
app.use('/api/users', [authMiddleware], usersRouter);
const {Hw3Error} = require('./utils/errors');

app.use((req, res, next) => {
    res.status(404).json({message: 'Not found'});
});

app.use((err, req, res, next) => {
    if (err instanceof Hw3Error) {
        return res.status(err.status).json({message: err.message});
    }
    res.status(500).json({message: err.message});
});

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://SashaF:SFtest@cluster0.cocva.mongodb.net/hwUber?retryWrites=true&w=majority', 
        {useNewUrlParser: true, useUnifiedTopology: true});

        app.listen(8080);
    } catch (err) {
        console.error(`Server error: ${err.message}`);
    }
    
}


start();