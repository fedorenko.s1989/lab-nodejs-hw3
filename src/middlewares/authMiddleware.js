const jwt = require('jsonwebtoken');

const { CredentialsError } = require('../utils/errors');

const authMiddleware = (req, res, next) => {
    const  {
        authorization
    } = req.headers;

    if (!authorization) {
        throw new CredentialsError('Plese, provide "authorization" header');
    }

    const [, token] = authorization.split(' ');

    if (!token) {
        throw new CredentialsError('Plese, provide "authorization" header');
    }

    try {
        const tokenPayload = jwt.verify(token, 'secret');
        
        req.user = {
            created_by: tokenPayload._id,
            role: tokenPayload.role,
            email: tokenPayload.email
        }
        next();
    } catch (err) {
        throw new CredentialsError('Plese, provide "authorization" header');
    }

}

module.exports = {
    authMiddleware
}