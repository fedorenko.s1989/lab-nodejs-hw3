class Hw3Error extends Error {
    constructor (message) {
        super(message);
        this.status = 500;
    }
}

class InvalidRequestError extends Hw3Error {
    constructor (message = 'Invalid request') {
        super(message);
        this.status = 400;
    }
}

class CredentialsError extends Hw3Error {
    constructor (message = 'Invalid credentials') {
        super(message);
        this.status = 400;
    }
}

class ValidationError extends Hw3Error {
    constructor (message = 'Invalid request') {
        super(message);
        this.status = 400;
    }
}

module.exports = {
    Hw3Error,
    InvalidRequestError,
    CredentialsError,
    ValidationError
}