const express = require('express');
const { Load } = require('../models/loadModel');
const router = express.Router();

const {
    getLoadsByUserId, 
    addLoadForUser,
    getUserActiveLoad,
    iterateToNextLoadState,
    getLoadByIdForUser,
    updateLoadByIdForUser,
    deleteLoadByIdForUser,
    postLoadByIdForUser,
    getLoadShippingInfoByIdForUser
    
 } = require('../services/loadsService');

const { asyncWrapper } = require('../utils/apiUtils');

const { InvalidRequestError, CredentialsError } = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;
    const { limit, offset } = req.params;
    const { status } = req.body;
    
    if (limit>50) {
        return limit = 50;
    }

    const loads = await getLoadsByUserId(created_by, role, limit, offset);

    if (status) {
        const filteredGogs = await loads.where({status: {$ne: status}});
        res.json({filteredGogs});
    }
    res.json({loads});
}));

router.post('/', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;

    if(role !== 'SHIPPER') {
        throw new CredentialsError('You do not have the required permissions');
    }
    await addLoadForUser(created_by, req.body);
    res.json({message: 'Load created successfully'});
}));

router.get('/active', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;
    
    if(role !== 'DRIVER') {
        throw new CredentialsError('You do not have the required permissions');
    }
    const load = await getUserActiveLoad(created_by);
    res.json({load});
}));

router.patch('/active/state', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;
    
    if(role !== 'DRIVER') {
        throw new CredentialsError('You do not have the required permissions');
    }
    const load = await iterateToNextLoadState(created_by);
    
    res.json({message: `Load state changed to "${load.state}"`});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;

    const { id } = req.params;
    const load = await getLoadByIdForUser(id, created_by, role);
    if(!load) {
        throw new InvalidRequestError('No loads found');
    }
    res.json({load});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;

    if(role !== 'SHIPPER') {
        throw new CredentialsError('You do not have the required permissions');
    }
    const { id } = req.params;
    const data = req.body;
    await updateLoadByIdForUser(id, created_by, data);
    
    res.json({message: 'Load details changed successfully'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;
    
    if(role !== 'SHIPPER') {
        throw new CredentialsError('You do not have the required permissions');
    }
    const { id } = req.params;
    await deleteLoadByIdForUser(id, created_by);
    res.json({message: 'Load deleted successfully'});
}));

router.post('/:id/post', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;

    if(role !== 'SHIPPER') {
        throw new CredentialsError('You do not have the required permissions');
    }
    const { id } = req.params;
    const truck = await postLoadByIdForUser(id, created_by);
    if (!truck) {
        throw new Error('Driver is not found');
    }

    res.json({
        message: 'Load posted successfully',
        driver_found: true
    });
}));

router.get('/:id/shipping_info', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;

    if(role !== 'SHIPPER') {
        throw new CredentialsError('You do not have the required permissions');
    }
    const { id } = req.params;
    const loadInfo = await getLoadShippingInfoByIdForUser(id, created_by);
    if(!loadInfo) {
        throw new InvalidRequestError('No load found');
    }
    res.json({loadInfo});
}));

module.exports = {
    loadsRouter: router
}