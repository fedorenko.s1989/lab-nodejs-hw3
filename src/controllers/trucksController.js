const express = require('express');
const { Truck } = require('../models/truckModel');
const router = express.Router();

const { 
    getTrucksByUserId,
    addTruckForUser,
    getTruckByIdForUser,
    updateTruckByIdForUser,
    deleteTruckByIdForUser,
    assignTruckByIdForUser
 } = require('../services/trucksService');

const { asyncWrapper } = require('../utils/apiUtils');

const { InvalidRequestError, CredentialsError } = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;
    
    if(role !== 'DRIVER') {
        throw new CredentialsError('You do not have the required permissions');
    }
    const trucks = await getTrucksByUserId(created_by);
    res.json({trucks});
}));

router.post('/', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;
    
    if(role !== 'DRIVER') {
        throw new CredentialsError('You do not have the required permissions');
    }
    await addTruckForUser(created_by, req.body);
    res.json({message: 'Truck created successfully'});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;

    if(role !== 'DRIVER') {
        throw new CredentialsError('You do not have the required permissions');
    }
    const { id } = req.params;
    const truck = await getTruckByIdForUser(id, created_by);
    if(!truck) {
        throw new InvalidRequestError('No trucks found');
    }
    res.json({truck});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;

    if(role !== 'DRIVER') {
        throw new CredentialsError('You do not have the required permissions');
    }
    const { id } = req.params;
    const data = req.body;
    await updateTruckByIdForUser(id, created_by, data);
    res.json({message: 'Truck details changed successfully'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;
    
    if(role !== 'DRIVER') {
        throw new CredentialsError('You do not have the required permissions');
    }
    const { id } = req.params;
    await deleteTruckByIdForUser(id, created_by);
    res.json({message: 'Truck deleted successfully'});
}));

router.post('/:id/assign', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;
    
    if(role !== 'DRIVER') {
        throw new CredentialsError('You do not have the required permissions');
    }

    const { id } = req.params;
    
    await assignTruckByIdForUser(id, created_by);
    res.json({message: 'Truck assigned successfully'});
}));

module.exports = {
    trucksRouter: router
}
