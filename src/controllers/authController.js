const express = require('express');
const router = express.Router();

const {
    registration,
    logIn
} = require('../services/authService');

const { asyncWrapper } = require('../utils/apiUtils');

router.post('/register', asyncWrapper(async (req, res) => {
    const {
        role,
        email,
        password
    } = req.body;

    await registration({role, email, password});

    res.json({message: 'Success'}); 
}));

router.post('/login', asyncWrapper(async (req, res) => {
    const {
        role,
        email,
        password
    } = req.body;

    const jwt_token = await logIn({role, email, password});

    res.json({message: 'Success', jwt_token});
}));

module.exports = {
    authRouter: router
}