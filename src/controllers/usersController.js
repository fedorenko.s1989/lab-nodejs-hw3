const express = require('express');
const { User } = require('../models/userModel');
const router = express.Router();

const { 
    getUserById,
    deleteUserById,
    changeUserPasswordById
 } = require('../services/usersService');

const { asyncWrapper } = require('../utils/apiUtils');

const { InvalidRequestError, CredentialsError } = require('../utils/errors');

router.get('/me', asyncWrapper(async (req, res) => {
    const { created_by } = req.user;
    
    const user = await getUserById(created_by);
    
    if(!user) {
        throw new InvalidRequestError('No users found');
    }
    res.json({user});
}));

router.delete('/me', asyncWrapper(async (req, res) => {
    const { created_by, role } = req.user;

    if(role === 'DRIVER') {
        throw new CredentialsError('You do not have the required permissions');
    }
    
    await deleteUserById(created_by);
    res.json({message: 'Profile deleted successfully'});
}));

router.patch('/me/password', asyncWrapper(async (req, res) => {
    const { created_by } = req.user;

    const { oldPassword, newPassword } = req.body;

    await changeUserPasswordById(created_by, oldPassword, newPassword);
    res.json({message: 'Password changed successfully'});
}));

module.exports = {
    usersRouter: router
}